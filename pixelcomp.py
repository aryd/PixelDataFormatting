#!/usr/bin/env python

import math
import sys
import random
import copy

from math import floor
from math import ceil
from math import log

def partitionbin(e,n,order):

    if len(e)==0 :
        return []

    if order[n-1]=='u' :

        size=4
        if use50by50:
            size=2
        
        for i in range(n,4) :
            if order[i]=='u' :
                size/=2
        
        u=[]  
        d=[]

        for pixel in e :
            upper = pixel[1]%size>=size/2 
            if upper :
                u.append(pixel)
            else :
                d.append(pixel)

        if n==1 :
            return [u,d]
        else :
            return [partitionbin(u,n-1,order),partitionbin(d,n-1,order)]

    if order[n-1]=='r' :

        size=4
        if use50by50:
            size=8

        
        for i in range(n,4) :
            if order[i]=='r' :
                size/=2
        
        l=[]  
        r=[]

        for pixel in e :
            right = pixel[0]%size>=size/2 
            if right :
                r.append(pixel)
            else :
                l.append(pixel)

        if n==1 :
            return [l,r]
        else :
            return [partitionbin(l,n-1,order),partitionbin(r,n-1,order)]

    print "Error should never get here. n =",n
    
    
def bitstringbin(epart,n) :

    if n == 0 :
        if len(epart)>1 :
            print "ERROR: epart to large",epart
        if len(epart)!=0 :
            return "cccc"
        return ""

    if len(epart) != 2 :
        print "Error wrong epart length in bitstringbin:",len(epart),epart
    
    str=""

    if len(epart[0])+len(epart[1])==0 :
        print "ERROR: Can not have both elements empty!!!"
        
    if len(epart[0])>0:
        str+="1"
        if len(epart[1])>0 :
            str+="1"
        else :
            str+="0"
        str+=bitstringbin(epart[0],n-1)
        if len(epart[1])>0 :
            str+=bitstringbin(epart[1],n-1)
    else:
        str+="0"
        str+=bitstringbin(epart[1],n-1)
        
    return str;


def pixelsbycolumn(pixels,colwidth) :

    colpixels=[]
    
    for col in range(0,55) :
        colpixels.append([])
        
    for pixel in pixels :
        icol=pixel[0]/colwidth
        if icol>=55 :
            print "ERROR wrong icol",icol,pixel[1]
            continue
        if icol<0 :
            print "Did not add pixel",icol,pixel
            continue
        colpixels[icol].append(pixel)

    return colpixels
    
def dataformatpixelmask(colpixels,use50by50) :

    str=""
    pcbs=[]

    partitionstr="urur"
    colwidth=4
    colheight=4
    if use50by50:
        colwidth=8
        colheight=2
        partitionstr="rurr"
    
    #colpixels=pixelsbycolumn(pixels,colwidth)

    for col in range(0,55) :
        colpartitioned=[]
        for irow in range(0,164) :
            colpartitioned.append([])
        for pixel in colpixels[col] :
            irow=pixel[1]/colheight
            colpartitioned[irow].append(pixel)
        #print "colpartitioned :",colpartitioned
        #pcbs.append(colpartitioned)
        nrows=0
        strtmp=""
        skip=False
        for irow in range(0,164) :
            if skip :
                skip=False
                continue
            if len(colpartitioned[irow])==0 :
                continue
            npix=len(colpartitioned[irow])
            npix2=0
            rows=1
            if (len(strtmp)>0) :
                strtmp+="1"   #We already had pixels added in this PCB need to flag that more is coming
            if irow<163 :
                if len(colpartitioned[irow+1])>0 :
                    npix2=len(colpartitioned[irow+1])
                    skip=True
                    rows=2
            if irow>128 :
                strtmp+="1rrrrrr"    # rows 128 to 163
            else :
                strtmp+="0rrrrrrr"   # rows 0 to 127
            nrows+=1
            if rows==1 :
                strtmp+="0"
            else :
                strtmp+="1"
            for i in range(0,rows) :
                #if len(colpartitioned[irow+i])==1 :
                #    strtmp+="0xxxcccc"
                #else :
                #    strtmp+="1mmmmmmmm"
                #    for i in range(0,len(colpartitioned[irow+i])) :
                #        strtmp+="cccc"
                epixels=colpartitioned[irow+i]
                part1=partitionbin(epixels,4,partitionstr)
                strtmp+=bitstringbin(part1,4)
        if (len(strtmp)>0) :
            strtmp+="0"   #We already had pixels added in this PCB need to flag that NO more is coming
        #print "strtmp :",nrows,strtmp
        if nrows==0 :
            str+="0"
        if nrows>0 :
            str+="1"
#            if nrows<4 :
#                str+="bb"  #1-3 are valid number of pixel regions. 00 Means that we will have more.   
#            else :
#                str+="00BBBBBBBB"
            str+=strtmp

    #print "str : ",str
    return str


def dataformatATLAS(colpixels,use50by50) :

    str=""
    pcbs=[]

    colwidth=4
    colheight=4
    if use50by50:
        colwidth=8
        colheight=2
    
    #print "pixels :",pixels

    #colpixels=pixelsbycolumn(pixels,colwidth)

    
    for col in range(0,55) :
        colpartitioned=[]
        for irow in range(0,656) :
            colpartitioned.append([])
        for pixel in colpixels[col] :
            irow=pixel[1]/colheight
            icol=pixel[0]%colwidth
            icol=icol/4
            if icol<0 or icol>1 :
                print "WRONG icol :",icol.pixel[0]
            colpartitioned[2*irow+icol].append(pixel)
        skip=False
        for irow in range(0,656) :
            if skip :
                skip=False
                continue
            if len(colpartitioned[irow])==0 :
                continue
            if irow<655 :
                if len(colpartitioned[irow+1]) > 0 :
                    skip=True
            str+="aaaaaaaaaaaaaaaax"+"mmmm"
            npix=len(colpartitioned[irow])
            for i in range(0,npix) :
                str+="cccc"
            if skip :
                str+="mmmm"
                npix=len(colpartitioned[irow+1])
                for i in range(0,npix) :
                    str+="cccc"
            

    return str

def bitsA(collist):
    if collist == []:
        return 0
    else:
        address = 18
        mincol=10000
        minrow=10000
        maxcol=-1
        maxrow=-1
        for pixel in collist:
            if pixel[1]<minrow:
                minrow=pixel[1]
            if pixel[1]>maxrow:
                maxrow=pixel[1]
            if pixel[0]<mincol:
                mincol=pixel[0]
            if pixel[0]>maxcol:
                maxcol=pixel[0]

        x = 2
        y = 2
        tot = (maxcol-mincol+1)*(maxrow-minrow+1)*4
        total = address + tot + x + y
        return total

def bitsB(collist):
    if collist == []:
        return 0
    else:
        numhita = len(collist)
        tot = 4*numhita
        address = 18
        mincol=10000
        minrow=10000
        maxcol=-1
        maxrow=-1
        for pixel in collist:
            if pixel[1]<minrow:
                minrow=pixel[1]
            if pixel[1]>maxrow:
                maxrow=pixel[1]
            if pixel[0]<mincol:
                mincol=pixel[0]
            if pixel[0]>maxcol:
                maxcol=pixel[0]
        numcol = maxcol-mincol+1
        numrow = maxrow-minrow+1
        x = 2
        y = 2
        hitmap = numcol*numrow
        total = address + tot + x + y + hitmap
        return total

def bitsC(collist):
    if collist == []:
        return 0
    else:
        numhita = len(collist)
        numhitbits = floor(log(numhita,2)) + 1
        tot = 4*numhita
        address = 18
        mincol=10000
        minrow=10000
        maxcol=-1
        maxrow=-1
        for pixel in collist:
            if pixel[1]<minrow:
                minrow=pixel[1]
            if pixel[1]>maxrow:
                maxrow=pixel[1]
            if pixel[0]<mincol:
                mincol=pixel[0]
            if pixel[0]>maxcol:
                maxcol=pixel[0]
        numcol = maxcol-mincol+1
        numrow = maxrow-minrow+1
        x = 2
        y = 2
        localadd = floor(log(numrow*numcol,2))*numhita
        
        total = numhitbits + address + tot + x + y + localadd
        return total
    
    
def bits(collist,formt) :
    if formt=="A" :
        return bitsA(collist)
    if formt=="B" :
        return bitsB(collist)
    if formt=="C" :
        return bitsC(collist)
    print "ERROR in bits",formt
    
def bitsused(clusters,formt) :
    total=0;
    for cluster in clusters:
        total+=bits(cluster,formt)
    return total
    
def vertsplit(acollist,formt):
    minrow=10000
    maxrow=-1
    for pixel in acollist :
        if pixel[1]<minrow:
            minrow=pixel[1]
        if pixel[1]>maxrow:
            maxrow=pixel[1]
    #print "minrow maxrow:",minrow,maxrow,len(acollist)
    abits = bits(acollist,formt)
    ncollist = [acollist]
    nbitslist = [abits]
    nrange = []
    for frac in [.1,.2,.3,.4,.5,.6,.7,.8,.9]:
        n = int((maxrow-minrow+1)*frac)
        if n > 0 and nrange.count(n) == 0:
            nrange.append(n)
    for nsplit in nrange:
        bcollist = []
        ccollist = []
        for pixel in acollist:
            if (pixel[1]>=nsplit+minrow) :
                bcollist.append(pixel)
            else :
                ccollist.append(pixel)
        bbits = bits(bcollist,formt)
        cbits = bits(ccollist,formt)
        ncollist.append([bcollist,ccollist])
        nbitslist.append(bbits+cbits)
    minbits = min(nbitslist)
    index = nbitslist.index(minbits)
    if index > 0:
        bcollist = ncollist[index][0]
        ccollist = ncollist[index][1]
        if bcollist == [] and ccollist == []:
            pass
        elif bcollist == []:
            ccollist = vertsplit(ccollist,formt)
            return ccollist
        elif ccollist == []:
            bcollist = vertsplit(bcollist,formt)
            return bcollist
        else:
            bcollist = vertsplit(bcollist,formt)
            ccollist = vertsplit(ccollist,formt)
            return bcollist + ccollist
    else:
        if maxrow-minrow + 1 > 4:
            nbitslist = nbitslist[1:]
            minbits = min(nbitslist)
            index = nbitslist.index(minbits)
            bcollist = ncollist[index + 1][0]
            ccollist = ncollist[index + 1][1]
            if bcollist == [] and ccollist == []:
                pass
            elif bcollist == []:
                ccollist = vertsplit(ccollist,formt)
                return ccollist
            elif ccollist == []:
                bcollist = vertsplit(bcollist,formt)
                return bcollist
            else:
                bcollist = vertsplit(bcollist,formt)
                ccollist = vertsplit(ccollist,formt)
                return bcollist + ccollist
        else:
            return [acollist]


def horsplit(acollist,formt):
    mincol=10000
    maxcol=-1
    for pixel in acollist :
        if pixel[0]<mincol:
            mincol=pixel[0]
        if pixel[0]>maxcol:
            maxcol=pixel[0]
    #print "mincol maxcol:",mincol,maxcol,len(acollist)
    abits = bits(acollist,formt)
    ncollist = [acollist]
    nbitslist = [abits]
    nrange = []
    nlist = [1,2,4]
    for n in nlist:
        if n < len(acollist):
            nrange.append(n)
    for nsplit in nrange:
        bcollist = []
        ccollist = []
        for pixel in acollist:
            if (pixel[0]>=nsplit+mincol) :
                bcollist.append(pixel)
            else :
                ccollist.append(pixel)
        #print "bcollist",bcollist
        #print "ccollist",ccollist
        bbits = bits(bcollist,formt)
        cbits = bits(ccollist,formt)
        ncollist.append([bcollist,ccollist])
        nbitslist.append(bbits+cbits)
        minbits = min(nbitslist)
        index = nbitslist.index(minbits)
        if index > 0:
            bcollist = ncollist[index][0]
            ccollist = ncollist[index][1]
            if bcollist == 0 and ccollist == 0:
                pass
            elif bcollist == 0:
                ccollist = horsplit(ccollist,formt)
                return ccollist
            elif ccollist == 0:
                bcollist = horsplit(bcollist,formt)
                return bcollist
            else:
                bcollist = horsplit(bcollist,formt)
                ccollist = horsplit(ccollist,formt)
                return bcollist + ccollist
        else:
            return [acollist]
    else:
        return [acollist]

        
def dataformat(roc,use50by50,formt) :
    newpccollist = []
    for collist in roc:
        if collist==[]:
            continue
        if not use50by50 :
            clusterlist = vertsplit(collist,formt)
            if clusterlist == None :
                print "Error clusterlist is None"
            newpccollist += clusterlist
        else :
            clusterlist = vertsplit(collist,formt)
            for cluster in clusterlist :
                hsplitlist = horsplit(cluster,formt)
                #print "newpccollist",newpccollist
                #print "hsplitlist",hsplitlist
                #print "cluster",cluster
                newpccollist += hsplitlist

    #print "Bits format",formt,":",bitsused(newpccollist,formt)
    return bitsused(newpccollist,formt)

def pixelregionsplit(collist,use50by50) :

    dict={}

    colsize=2
    rowsize=2
    if use50by50 :
        colsize=1
        rowsize=4
    
    for pixel in collist :
        icol=pixel[0]/colsize
        irow=pixel[1]/rowsize
        regionadd=(icol,irow)
        if regionadd in dict :
            dict[regionadd].append(pixel)
        else :
            dict[regionadd]=[pixel]

    prlist=[]       
    for pr in dict :
        prlist.append(pr)
        
    return prlist

def bitsusedpixelregion(prlist) :
    address=16
    size=16
    return (address+size)*len(prlist)


def pixelregion(roc,use50by50) :
    newpccollist = []
    for collist in roc:
        if collist==[]:
            continue
        clusterlist = pixelregionsplit(collist,use50by50)
        if clusterlist != None:
            newpccollist += clusterlist
    return bitsusedpixelregion(newpccollist)



fp = open("pixelcompressed.dat","w")


use50by50=False

layer=-1

emptyroc=[]
for i in range(0,55) :
    emptyroc.append([])

inputfiles=["data_module_col_row.txt"]
if use50by50:
    inputfiles=["data_module_col_row_50x50.txt"]

for file in inputfiles:

    module2by2=False

    nrocs=0
    npixels=0
    nbits=0
    
    print "Will process :",file
    
    fi =  open(file,"r")

    count=0;

    ladder=0
    layer=0
    disk=0
    ring=0

    first=True;

    rocs=[]
    
    for line in fi:
        if "layer" in line :
            if first :
                first=False
            else :
                iroc=0
                for roc in rocs :
                    iroc+=1
                    npix=0
                    for col in roc :
                        npix+=len(col)
                    bitstratlas=dataformatATLAS(roc,use50by50)
                    bitstrpixelmask=dataformatpixelmask(roc,use50by50)
                    fmtasize=dataformat(roc,use50by50,"A")
                    fmtbsize=dataformat(roc,use50by50,"B")
                    fmtcsize=dataformat(roc,use50by50,"C")
                    prsize=pixelregion(roc,use50by50)
                    ccount=0
                    for c in bitstrpixelmask :
                        if c=="c" :
                            ccount+=1
                    #print "pixels from ccount =",ccount/4
                    if ccount/4!=npix :
                        print "Warning ccount and len(npix) dont agree";

                    print count,layer

                    if (layer>0) :
                        fp.write("L "+str(layer)+" "+str(ladder))
                    if (disk>0) :
                        diskstr="Dinner"
                        if iroc==1 or iroc==3 :
                            diskstr="Douter"
                        fp.write(diskstr+" "+str(disk)+" "+str(ring))

                    fp.write(" "+str(npix)+" "+str(22*npix)+" "+str(prsize))
                    fp.write(" "+str(fmtasize)+" "+str(fmtbsize))
                    fp.write(" "+str(fmtcsize))
                    fp.write(" "+str(len(bitstratlas))+" "+str(len(bitstrpixelmask))+"\n")

        if "side" in line :
            disk=int(line.split()[3])
            ring=int(line.split()[5])
            if disk>0 :
                if ring>=3 :
                    module2by2=True
                else :
                    module2by2=False
            if disk>0 :        
                layer=0
                ladder=0
            nrocs=2
            if module2by2 :
                nrocs+=2
            rocs=[]
            for i in range(0,nrocs):
                roc=copy.deepcopy(emptyroc)
                rocs.append(roc)
            print "len(rocs) ",len(rocs)
            continue
        if "layer" in line :
            layer=int(line.split()[1])
            ladder=int(line.split()[3])
            if layer>0 :
                if layer>=3 :
                    module2by2=True
                else :
                    module2by2=False                    
            if layer>0 :
                disk=0
                ring=0
            count+=1
            continue
                    

        sub = line.split()
        col=int(sub[0])
        row=int(sub[1])
        #print "row",row
        #print sub,x,y
        colwidth=220
        rows=656
        colsinpc=4
        if use50by50 :
            colsinpc=8
            colwidth=440
            rows=328
        if col>=2*colwidth :
            continue
        iroc=0
        if col>=colwidth:
            col-=colwidth
            iroc+=1        
        if row>=rows :
            if row>=2*rows :
                continue
            row-=rows
            iroc+=2
            
        pixel=(col,row)
        #print "iroc col :",iroc,col/colsinpc
        rocs[iroc][col/colsinpc].append(pixel)
            
        if count>100000 :
            break

    fi.close()
    
fp.close()

    
    
#print partitioned
