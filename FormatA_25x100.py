#Michael McGinnis
#7/27/18
#File with ROC data for Format A(from professor Ryd)

#Output:
#L 4 32 11 167 5       Layer 4, ladder 32, 11 hit pixels, 167 is the formatted size, 5 clusters/envelopes of pixels
#Douter 1 1 51 728 5   Outer ROC in Module (cols<220), disk 1, ring 1, 51 hit pixels, 728 formatted size, 5 clusters/envelopes of pixels

from math import floor
from math import ceil
from math import log

def bits(collist):
    if collist == 0:
        return 0
    else:
        address = 18
        numcol = len(collist)
        numrow = len(collist[0])
        x = 2
        y = 2
        tot = numcol*numrow*4
        total = address + tot + x + y
        return total

def reduce(collist):
    collist3 = []
    colindexlist = []
    rowindexlist = []
    colit = 0
    while colit < len(collist):
        rowlist = collist[colit]
        if rowlist.count(1) != 0:
            index = rowlist.index(1)
            rowlist.reverse()
            rindex = rowlist.index(1)
            rowlist.reverse()
            rowindexlist.append(index)
            rowindexlist.append(len(rowlist) - 1 - rindex)
            colindexlist.append(colit)
        colit += 1
    if colindexlist == []:
        return 0
    else:
        mincol = min(colindexlist)
        maxcol = max(colindexlist)
        minrow = min(rowindexlist)
        maxrow = max(rowindexlist)
        collist2 = collist[mincol:maxcol + 1 ]
        for rowlist2 in collist2:
            rowlist3 = rowlist2[minrow:maxrow + 1]
            collist3.append(rowlist3)
        return collist3
    
def vertsplit(acollist):
    if acollist == 0:
        pass
    elif acollist == [[[1]]]:
        return acollist
    else:
        abits = bits(acollist)
        ncollist = [acollist]
        nbitslist = [abits]
        nrange = []
        for frac in [.1,.2,.3,.4,.5,.6,.7,.8,.9]:
            n = int(len(acollist[0])*frac)
            if n > 0 and nrange.count(n) == 0:
                nrange.append(n)
        for nsplit in nrange:
            bcollist = []
            ccollist = []
            for arowlist in acollist:
                browlist = arowlist[:nsplit]
                crowlist = arowlist[nsplit:]
                bcollist.append(browlist)
                ccollist.append(crowlist)
            bcollist = reduce(bcollist)
            ccollist = reduce(ccollist)
            bbits = bits(bcollist)
            cbits = bits(ccollist)
            ncollist.append([bcollist,ccollist])
            nbitslist.append(bbits+cbits)
        minbits = min(nbitslist)
        index = nbitslist.index(minbits)
        if index > 0:
            bcollist = ncollist[index][0]
            ccollist = ncollist[index][1]
            if bcollist == 0 and ccollist == 0:
                pass
            elif bcollist == 0:
                ccollist = vertsplit(ccollist)
                return ccollist
            elif ccollist == 0:
                bcollist = vertsplit(bcollist)
                return bcollist
            else:
                bcollist = vertsplit(bcollist)
                ccollist = vertsplit(ccollist)
                return bcollist + ccollist
        else:
            if len(acollist[0]) > 4:
                nbitslist = nbitslist[1:]
                minbits = min(nbitslist)
                index = nbitslist.index(minbits)
                bcollist = ncollist[index + 1][0]
                ccollist = ncollist[index + 1][1]
                if bcollist == 0 and ccollist == 0:
                    pass
                elif bcollist == 0:
                    ccollist = vertsplit(ccollist)
                    return ccollist
                elif ccollist == 0:
                    bcollist = vertsplit(bcollist)
                    return bcollist
                else:
                    bcollist = vertsplit(bcollist)
                    ccollist = vertsplit(ccollist)
                    return bcollist + ccollist
            else:
                return [acollist]

def split(pccollist):
    newpccollist = []
    for collist in pccollist:
        collist = reduce(collist)
        collist = vertsplit(collist)
        if collist != None:
            newpccollist += collist
    return newpccollist

def bitsused(envlist):
    address = 18*len(envlist)
    x = 0
    y = 0
    tot = 0
    pccol = 0
    for collist in envlist:
        numcol = len(collist)
        numrow = len(collist[0])
        x += 2
        y += 2
        tot += numcol*numrow*4
        pccol += 1
    total = address + x + y + tot
    return total

#outputfile = open("formatA_25x100_output.txt", "r+")
#outputfile.truncate(0)
#outputfile.close()
datafile = "data_module_col_row.txt"

f = open(datafile, "r")                                #a list of (col,row) ordered pairs
fcopy = f.read()
f.close
maxmodule = 1000
print ("maxmodule: ", maxmodule)
usemodulelist = []

with open(datafile) as f:
    module = 'a'
    for line in f:
        if len(usemodulelist) < maxmodule:
            if line[0] == 'l':
                if len(module) == 1:
                    module = line
                else:
                    usemodulelist.append(module)
                    module = line
            else:
                module += line
        else:
            break

module4x1 = False                   #False is only 1X2 modules (L1, inner rings)
nummod = 3
nummodule = 1
for module in usemodulelist:
    modulelabel = ""
    module4x1 = False                   #False is only 1X2 modules (L1, inner rings)
    nummod = 3
    print nummodule
    nummodule += 1
    isdisk = False
    isside2 = False
    layer = int(module[8:9])
    if layer != 0:
        ladderstart = module.find("ladder")
        ladder = module[19:21].strip()
        modulelabel = "L " + str(layer) + " " + ladder
        layer4x1 = [3,4]
        if layer in layer4x1:
            module4x1 = True
    else:
        isdisk = True
        sidestart = module.find("side")
        side = int(module[sidestart + 6: sidestart + 7])
        if side == 1:
            disk = module[sidestart + 14:sidestart + 16].strip()
            bladestart = module.find("blade")
            blade = module[bladestart + 7:bladestart + 8]
            modulelabel = disk + " " + blade
            blade4x1 = ['3','4','5']
            if blade in blade4x1:
                module4x1 = True
        else:
            isside2 = True
    if not(isside2):
        modulelist = []
        roclist1 = []                                   #list of ordered pairs specific to a ROC specific to a module
        roclist2 = []                                   #roclist3 and roclist4 may be empty if 1x2 module
        roclist3 = []
        roclist4 = []
        orderedpairlist = []                             #list of ordered pairs specific to one module
        startmod = module.find("\n", 45)
        module = module[startmod + 1:]
        for pair in module:
            start = 0
            startnext = module.find("\n")
            if startnext == -1:
                startnext = len(module.strip("\n")) + 1
            pair = module[start:startnext]
            module = module[startnext + 1:]
            
            colstart = 0
            rowstart = pair.find(" ") + 1
            col = pair[colstart:rowstart - 1]
            row = pair[rowstart:]
            if row == '':
                break
            orderedpairlist.append([col,row])
    
        p = 0
        while p < len(orderedpairlist):                         #in data, col and row start with 0
            pair = orderedpairlist[p]
            col = int(pair[0])
            row = int(pair[1])
            if col > 439 or row > 1311:
                    pass
            elif col < 220:
                if row < 656:
                    roclist1.append(pair)
                else:
                    roclist3.append(pair)
            elif col > 219:
                if row < 656:
                    roclist2.append(pair)
                else:
                    roclist4.append(pair)
            p +=1
            
        modulelist.append(roclist1)
        modulelist.append(roclist2)
        if module4x1:
            modulelist.append(roclist3)
            modulelist.append(roclist4)
        
        if module4x1:
            nummod = 5
        label = modulelabel
        q = 1
        while q < nummod:
            label = modulelabel
            if isdisk:
                if q in [1,3]:
                    label = "Douter " + label
                else:
                    label = "Dinner " + label
            roc = modulelist[q-1]                                     #only does one module, p picks which index in the list of modules, and q picks the ROC
            if q == 1:
                qx = 0
                qy = 0
            elif q == 2:
                qx = 1
                qy = 0
            elif q == 3:
                qx = 0
                qy = 1
            elif q == 4:
                qx = 1
                qy = 1
            numhit = len(roc)
            label += " " + str(numhit)
            pccollist = []
            pccolit = 0
            while pccolit < 55:
                collist = []
                colit = 0
                while colit < 4:
                    rowlist = []
                    rowit = 0
                    while rowit < 656:
                        rowlist.append(0)
                        rowit += 1
                    collist.append(rowlist)
                    colit += 1
                pccollist.append(collist)
                pccolit += 1
            n = 0
            while n < len(roc):
                hit = roc[n]
                hitx = int(hit[0])
                hity = int(hit[1])
                nrowit = hity
                if qy == 1:
                    nrowit -= 656
                if qx == 1:
                    hitx -= 220
                npccolit = hitx/4
                ncolit = hitx - npccolit*4
                pccollist[npccolit][ncolit][nrowit] = 1
                n += 1
            
            envlist = split(pccollist)
            numbits = bitsused(envlist)
            label += " " + str(numbits)
            label += " " + str(len(envlist))
            outputfile = open("formatA_25x100_output.txt", "a")
            outputfile.write(label + "\n")
            outputfile.close()
            q += 1
