#!/usr/bin/env python

import math
import sys
import random

formatdict={
    "PixelRegion":5,
    "FormatA":6,
    "FormatB":7,
    "FormatC":8,
    "ATLAS":9,
    "PixelMask":10
}

fmt="PixelRegion"

index=formatdict[fmt]

fi = open("pixelcompressed_50by50.dat","r")

regions=["TBPX:L1:Inner","TBPX:L1:Outer","TBPX:L2","TBPX:L3","TBPX:L4","TFPX:R1:Inner","TFPX:R1:Outer","TFPX:R2:Inner","TFPX:R2:Outer","TFPX:R3:Inner","TFPX:R3:Outer","TFPX:R4:Inner","TFPX:R4:Outer","TEPX:R1:Inner","TEPX:R1:Outer","TEPX:R2:Inner","TEPX:R2:Outer","TEPX:R3:Inner","TEPX:R3:Outer","TEPX:R4:Inner","TEPX:R4:Outer","TEPX:R5:Inner","TEPX:R5:Outer"]

data=[]

for region in regions:
    dataitem=[0,0,0]
    data.append(dataitem)
        


for line in fi:
    
    modstr=line.split()[0]
    layerdisk=int(line.split()[1])
    ladderring=int(line.split()[2])
    npix=int(line.split()[3])
    nbits=float(line.split()[index])

    name=""

    if "L" in modstr :
        name="TBPX:L"+str(layerdisk)
        if layerdisk==1 :
            if ladderring%2==0 :
                name+=":Inner"
            else :
                name+=":Outer"

    if "D" in modstr :
        if layerdisk<9:
            name="TFPX:R"+str(ladderring)
        else :
            name="TEPX:R"+str(ladderring)
        if "inner" in modstr :
            name+=":Inner" 
        else :
            name+=":Outer" 
             

    if name=="" :
        continue

    #print "name :",name

    i=0;
    for region in regions :
        if region == name :
            dataitem=data[i]
            dataitem[0]+=1
            dataitem[1]+=npix
            dataitem[2]+=nbits
            data[i]=dataitem
        i+=1

i=0;

print "Data volume summary for",fmt
print "Det. Region        Nroc     Avg. #      Raw size   Format size   Data  Data Rate  N-elink      Link"
print "                            Pixels        (bits)        (bits) Reduct.  (Gbits/s)          Occ. (%)"
print "========================================================================================================================"

for region in regions :
    dataitem=data[i]
    nelink=-1
    if "L1" in region:
        nelink=3
    if "L2" in region:
        nelink=1
    if "L3" in region:
        nelink=0.5
    if "L4" in region:
        nelink=0.25
    if "TFPX" in region:
        if "R1:Inner" in region:
            nelink=2
        if "R1:Outer" in region:
            nelink=1
        if "R2" in region:
            nelink=1
        if "R3" in region:
            nelink=0.5
        if "R4" in region:
            nelink=0.25
    if "TEPX" in region:
        if "R1" in region:
            nelink=0.5
        if "R2" in region:
            nelink=0.5
        if "R3" in region:
            nelink=0.25
        if "R4" in region:
            nelink=0.25
        if "R5" in region:
            nelink=0.25

    nrocs=dataitem[0]
    npixels=dataitem[1]
    nbits=dataitem[2]

    rawbits=22*npixels

    datarate=nbits*0.00075/nrocs

    linkocc=datarate/(1.28*nelink)

    nrocsi=0
    npixelsi=0
    nbitsi=0

    if "Outer" in region :
        dataitemi=data[i-1] 
        nrocsi=dataitemi[0]
        npixelsi=dataitemi[1]
        nbitsi=dataitemi[2]


            
    star=" "
    if linkocc>0.75 :
        star="*"

    if "L" in region or nelink>=1 :    
        print('{0:17s} {1:5d} {2:10.1f} {3:13.1f} {4:13.1f} {5:7.1f} {6:10.2f} {7:7.2f} {8:9.1f}{9:1s} '.format(region, nrocs, float(npixels)/nrocs, float(rawbits)/nrocs, float(nbits)/nrocs, float(rawbits)/nbits, datarate, nelink, linkocc*100,star))
    else :
        if "Inner" in region :
            print('{0:17s} {1:5d} {2:10.1f} {3:13.1f} {4:13.1f} {5:7.1f} {6:10.2f} {7:7.2f}'.format(region, nrocs, float(npixels)/nrocs, float(rawbits)/nrocs, float(nbits)/nrocs, float(rawbits)/nbits, datarate, nelink))
        else :
            dataratei=nbitsi*0.00075/nrocsi
            if nrocs!=nrocsi :
                 print "nrocs not equal to nrocsi :",nrocs,nrocsi
            linkocci=dataratei/(1.28*nelink)
            linkoccavg=0.5*(linkocc+linkocci)
            star=" "
            if linkoccavg>0.75 :
                 star="*"
            print('{0:17s} {1:5d} {2:10.1f} {3:13.1f} {4:13.1f} {5:7.1f} {6:10.2f} {7:7.2f} {8:9.1f}{9:1s} '.format(region, nrocs, float(npixels)/nrocs, float(rawbits)/nrocs, float(nbits)/nrocs, float(rawbits)/nbits, datarate, nelink, linkoccavg*100,star))
            
    i+=1
         
fi.close()

    
    
#print partitioned
